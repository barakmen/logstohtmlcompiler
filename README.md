# FSMEventsToHTMLCompiler

It's a compiler that takes a log of FSM events and outputs an HTML view of it.


## The CFG for the log is:
    NAME -> (A-Z | a-z)+(A-Z | | a-z | _ | 0-9)*
    NEWLINE -> \n
    ~STATE -> [ NAME ] | TRANSITION
    EVENT -> { NAME }
    TRANSITION -> STATE + NEWLINE + EVENT + NEWLINE + STATE




```mermaid
graph LR
    ConnectionSucces -->|1 - Link text| PERFECT
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    B --> C{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    C --> B{Decision}
    PERFECT --> B

    C -->|One| D[Result one]
    B -->|One| D[Result one]
    C -->|Two| E[Result two]
```







DDD