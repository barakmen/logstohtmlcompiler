from rply import LexerGenerator


class Lexer():
    def __init__(self):
        self.lexer = LexerGenerator()

    def _add_tokens(self):
        # Parenthesis
        self.lexer.add('STATE_OPEN_PAREN', r'\[')
        self.lexer.add('STATE_CLOSE_PAREN', r'\]')
        self.lexer.add('EVENT_OPEN_PAREN', r'\{')
        self.lexer.add('EVENT_CLOSE_PAREN', r'\}')

        # Transition
        self.lexer.add('TANSITION', r'\-\>')

        # Number
        self.lexer.add('NAME', r'([a-z]|[A-Z])+(\d+|([a-z]|[A-Z]))*')

        # Ignore spaces
        self.lexer.ignore('\s+')

    def get_lexer(self):
        self._add_tokens()
        return self.lexer.build()
