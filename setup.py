import setuptools
with open("README.md", "r") as fh:
    long_description = fh.read()
setuptools.setup(
    name='logscompiler',
    version='0.1',
    author="barak menachem",
    author_email="barak8807@gmail.com",
    description="It's a compiler that takes a log of FSM events and outputs an HTML view of it.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3.7",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
    ],
)
